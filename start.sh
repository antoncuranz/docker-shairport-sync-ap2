#!/bin/sh

mkdir -p /var/run/dbus

dbus-uuidgen --ensure
dbus-daemon --system

avahi-daemon --daemonize --no-chroot
nqptp &

shairport-sync -vu -a "$AIRPLAY_NAME" "$@"
