FROM ubuntu

RUN apt update
RUN DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends build-essential git xmltoman autoconf automake libtool \
    libpopt-dev libconfig-dev libasound2-dev avahi-daemon libavahi-client-dev libssl-dev libsoxr-dev libglib2.0-dev \
    libplist-dev libsodium-dev libavutil-dev libavcodec-dev libavformat-dev uuid-dev libgcrypt-dev xxd

# install nqptp
WORKDIR /
RUN git clone https://github.com/mikebrady/nqptp.git
WORKDIR /nqptp
RUN autoreconf -fi
RUN ./configure
RUN make
RUN make install

# install shairport-sync with airplay2
WORKDIR /
RUN git clone https://github.com/mikebrady/shairport-sync.git
WORKDIR /shairport-sync
RUN git checkout development
RUN autoreconf -fi
RUN ./configure --sysconfdir=/etc --with-alsa \
    --with-soxr --with-avahi --with-ssl=openssl --with-dbus-interface --with-airplay-2
RUN make -j
RUN make install

RUN apt install -y alsa-base

COPY start.sh /
ENTRYPOINT [ "/start.sh" ]
